/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marchapp.mensajes_app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author olivermarchruiz
 */
public class Conexion {
    
    public Connection get_connection(){
        Connection conection = null;
            
        try{
            conection = DriverManager.getConnection("jdbc:mysql://localhost:3306/northwind","root","1q2w3e4r");
            
            if(conection != null){
                System.out.println("Conexión realizada con éxito");
            }
                  
        }catch(SQLException e){
            System.out.println(e);
        }
        return conection;
    } 
    
}
